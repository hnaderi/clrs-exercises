function maximumSubarrayDemo () {
  var width,
      height = 450,
      cellWidth;

  //var randomValues;

  var x;

  var y=function y(d) {
    return height-d;
  }
  var bars;
  var color=d3.scale.sqrt()
              .domain([0, height])
              .range(["rgb(255,0,0)", "rgb(100,255,0)"])
              .interpolate(d3.interpolateRgb);

  var line = d3.svg.line()
      .x(function(d,i) { return x(i); })
      .y(function(d,i) { return y(d); });

  function loadData (n) {
    randomValues = d3.range(n).map(function (i) {
      return ((Math.random()*height)+10);
    });
  }

  function render(){
    var n=randomValues.length;
    $("svg").empty();
    width= $("#view").width();

    x=d3.scale.linear()
          .domain([0,n])
          .range([0,width]);

    cellWidth=Math.floor(width/n);

    if(n<=1000){
      $("#visualisation-disabled").hide();
      renderArray();
    }
    else{
      $("#visualisation-disabled").show();
    }
  }

  function renderArray() {
    var svg=d3.select("svg")
        .attr("width", width)
        .attr("height", height);

    if(cellWidth>=1){
      bars=svg.append("g")
        .selectAll("rect")
        .data(randomValues)
        .enter()
        .append("rect")
        .attr("height",0)
        .transition()
        .ease("elastic")
        .duration(1000)
        .delay(function (d,i) {
          return i*cellWidth;
        })
        .attr("x",function (d,i) { return x(i); })
        .attr("y",function (d,i) { return y(d); })
        .attr("height",function (d) { return d; })
        .attr("width",cellWidth)
        .style("fill",function (d) {
          return color(d);
        });
    }
    else{
      svg.append("path")
        .datum(randomValues)
        .attr("class", "line")
        .attr("d",line);
    }
  }

  function process () {
    var changes=randomValues.map((v,i,a)=>(i<a.length)?(a[i+1]-a[i]):Infinity);
    changes[changes.length-1]=-Infinity;

    var t1=performance.now();
    var max=findMaximumSubarray(changes);
    var t2=performance.now();

    logResult("DC:",t2-t1,max);

    highlightSubarray(max);
  }

  function highlightSubarray (subarray) {

      d3.select("svg")
      .append("line")
      .attr("y1",0)
      .attr("y2",0)
      .attr("x1",0)
      .attr("x2",x(width))
      .transition()
      .ease("elastic")
      .duration(1000)
      .attr("x1",x(subarray.start))
      .attr("x2",x(subarray.end+1)) // one for width
      .attr("y1",height-2)
      .attr("y2",height-2);

      d3.select("svg")
        .selectAll("rect")
        .transition()
          .ease("elastic")
          .duration(1000)
          .style("opacity","0.5");
      d3.selectAll([bars[0][subarray.start],bars[0][subarray.end]])
        .transition()
          .ease("elastic")
          .duration(1000)
          .style("opacity","1")
          .attr("width",function(d){ return (cellWidth<5) ? "5px" : (cellWidth+"px") });
  }

  function processBF() {

    var t1=performance.now();
    var max=bruteForce();
    var t2=performance.now();

    logResult("BF:",t2-t1,max);

    highlightSubarray(max);
  }

  function findMaximumSubarray (arr,start,end) {
    start=start||0;
    end=(end==undefined)?(arr.length-1):(end);
    
    if(start==end)
      return Subarray(start,end+1,arr[start]);

    var maxSubArr;
    var mid=Math.floor((start+end)/2);
    var left=findMaximumSubarray(arr,start,mid);
    var right=findMaximumSubarray(arr,mid+1,end);
    var cross=findCrossedMax(arr,start,mid,end);

    return [left,cross,right].reduce(function (a,b) {
      return a.sum>b.sum?a:b;
    })
  }   

  function findCrossedMax (arr,low,mid,high) {
    var leftSum=-Infinity;
    var leftIndex=mid;
    var sum=0;
    for(var i=mid;i>=low;--i){
      sum=sum+arr[i];
      if(sum>leftSum){
        leftSum=sum;
        leftIndex=i;
      }
    }

    var rightSum=-Infinity;
    var rightIndex=mid;
    sum=0;
    for(var i=mid+1;i<=high;++i){
      sum=sum+arr[i];
      if(sum>rightSum){
        rightSum=sum;
        rightIndex=i;
      }
    }
    
    if(leftIndex==rightIndex)
      return Subarray(leftIndex,leftIndex,-Infinity);

    return Subarray(leftIndex,rightIndex+1,leftSum+rightSum);
  }

  function Subarray (start,end,sum) {
    return { start:start , end:end , sum:sum };
  }

  function bruteForce () {
    var res=Subarray(0,0,0);
    var sum=0;
    for(var i=0;i<randomValues.length;++i){
      for (var j = i; j <randomValues.length; j++) {
        sum =randomValues[j]-randomValues[i];
        if(sum>res.sum){
          res.start=i;
          res.end=j;
          res.sum=sum;
        }
      };
    }
    return res;
  }
  function logResult (msg,time,result) {
   $("#log").append("<h6>"+msg+time.toString()+" ms "+JSON.stringify(result)+"</h6>");
  }

  return {
    load:function (n) {
      loadData(n);
      render();
    },
    showDemo:process,
    showDemoBF:processBF,
    refresh:render
  }
};